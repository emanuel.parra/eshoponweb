﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using MinimalApi.Endpoint;

namespace Microsoft.eShopWeb.PublicApi.CatalogItemEndpoints;

/// <summary>
/// Get Catalog Items by Localidad
/// </summary>
public class ListProdLocalidadEndpoint : IEndpoint<IResult, ListProdLocalidadRequest, IRepository<CatalogItem>>
{
    private readonly IUriComposer _uriComposer;

    public ListProdLocalidadEndpoint(IUriComposer uriComposer)
    {
        _uriComposer = uriComposer;
    }

    public void AddRoute(IEndpointRouteBuilder app)
    {
        app.MapGet("api/catalog-items/localidad/{localidad}",
            async (int? localidad, IRepository<CatalogItem> itemRepository) =>
            {
                return await HandleAsync(new ListProdLocalidadRequest(localidad), itemRepository);
            })
            .Produces<ListProdLocalidadResponse>()
            .WithTags("CatalogItemEndpoints");
    }

    public async Task<IResult> HandleAsync(ListProdLocalidadRequest request, IRepository<CatalogItem> itemRepository)
    {
        var response = new ListProdLocalidadResponse(request.CorrelationId());

        var items = await itemRepository.ListAsync(new CatalogItemsByLocalidadSpecification(request.Localidad));

        response.CatalogItems = items.Select(item => new CatalogItemDto
        {
            Id = item.Id,
            CatalogBrandId = item.CatalogBrandId,
            CatalogTypeId = item.CatalogTypeId,
            CatalogLocalidadId = item.CatalogLocalidadId.Value,
            Description = item.Description,
            Name = item.Name,
            PictureUri = _uriComposer.ComposePicUri(item.PictureUri),
            Price = item.Price
        }).ToList();

        return Results.Ok(response);
    }
}




