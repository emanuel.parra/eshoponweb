﻿using Microsoft.eShopWeb.PublicApi.CatalogItemEndpoints;
using Microsoft.eShopWeb.PublicApi;
using System.Collections.Generic;
using System;

public class ListProdLocalidadResponse : BaseResponse
{
    public List<CatalogItemDto> CatalogItems { get; set; } = new List<CatalogItemDto>();

    public ListProdLocalidadResponse(Guid correlationId) : base(correlationId)
    {
    }

    public ListProdLocalidadResponse()
    {
    }
}
