﻿using Microsoft.eShopWeb.PublicApi;

public class ListProdLocalidadRequest : BaseRequest
{
    public int? Localidad { get; init; }

    public ListProdLocalidadRequest(int? localidad)
    {
        Localidad = localidad;
    }
}
