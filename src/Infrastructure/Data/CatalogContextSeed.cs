﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.Extensions.Logging;

namespace Microsoft.eShopWeb.Infrastructure.Data;

public class CatalogContextSeed
{
    public static async Task SeedAsync(CatalogContext catalogContext,
        ILogger logger,
        int retry = 0)
    {
        var retryForAvailability = retry;
        try
        {
            if (catalogContext.Database.IsSqlServer())
            {
                catalogContext.Database.Migrate();
            }
            await SeedCatalogBrands(catalogContext);

            // Seed CatalogTypes
            await SeedCatalogTypes(catalogContext);

            // Seed Localidades
            await SeedLocalidades(catalogContext);

            // Seed CatalogItems
            await SeedCatalogItems(catalogContext);


        }
        catch (Exception ex)
        {
            if (retryForAvailability >= 10) throw;

            retryForAvailability++;

            logger.LogError(ex.Message);
            await SeedAsync(catalogContext, logger, retryForAvailability);
            throw;
        }
    }

    private static async Task SeedCatalogBrands(CatalogContext catalogContext)
    {
        if (!await catalogContext.CatalogBrands.AnyAsync())
        {
            await catalogContext.CatalogBrands.AddRangeAsync(GetPreconfiguredCatalogBrands());
            await catalogContext.SaveChangesAsync();
        }
    }


    private static async Task SeedCatalogTypes(CatalogContext catalogContext)
    {
        if (!await catalogContext.CatalogTypes.AnyAsync())
        {
            await catalogContext.CatalogTypes.AddRangeAsync(GetPreconfiguredCatalogTypes());
            await catalogContext.SaveChangesAsync();
        }
    }


    private static async Task SeedCatalogItems(CatalogContext catalogContext)
    {
        if (!await catalogContext.CatalogItems.AnyAsync())
        {
            await catalogContext.CatalogItems.AddRangeAsync(GetPreconfiguredItems());
            await catalogContext.SaveChangesAsync();
        }

        var localidadProductoMap = new Dictionary<string, List<string>>
            {
                { "Quito", new List<string> { "Roslyn Red Sheet", ".NET Bot Black Sweatshirt", ".NET Blue Sweatshirt" } },
                { "Guayaquil", new List<string> { "Cup<T> White Mug", "Roslyn Red T-Shirt" } }
            };

        foreach (var localidad in localidadProductoMap)
        {
            var localidadEntity = await catalogContext.CatalogLocalidades.FirstOrDefaultAsync(l => l.Localidad == localidad.Key);
            if (localidadEntity != null)
            {
                foreach (var productName in localidad.Value)
                {
                    var catalogItem = await catalogContext.CatalogItems.FirstOrDefaultAsync(ci => ci.Name == productName);
                    if (catalogItem != null)
                    {
                        catalogItem.UpdateLocalidad(localidadEntity.Id);
                    }
                }
            }
        }

        var defaultLocalidad = "Default";
        var productsNotMapped = await catalogContext.CatalogItems
            .ToListAsync(); // Realiza la evaluación en memoria

        var defaultLocalidadEntity = await catalogContext.CatalogLocalidades.FirstOrDefaultAsync(l => l.Localidad == defaultLocalidad);
        if (defaultLocalidadEntity != null)
        {
            foreach (var product in productsNotMapped)
            {
                if (!localidadProductoMap.Values.SelectMany(list => list).Contains(product.Name))
                {
                    product.UpdateLocalidad(defaultLocalidadEntity.Id);
                }
            }
        }

        await catalogContext.SaveChangesAsync();
    }

    private static async Task SeedLocalidades(CatalogContext catalogContext)
    {
        if (!await catalogContext.CatalogLocalidades.AnyAsync())
        {
            await catalogContext.CatalogLocalidades.AddRangeAsync(GetPreconfiguredLocalidades());
            await catalogContext.SaveChangesAsync();
        }
    }

    static IEnumerable<CatalogBrand> GetPreconfiguredCatalogBrands()
    {
        return new List<CatalogBrand>
            {
                new("Azure"),
                new(".NET"),
                new("Visual Studio"),
                new("SQL Server"),
                new("Other")
            };
    }

    static IEnumerable<CatalogType> GetPreconfiguredCatalogTypes()
    {
        return new List<CatalogType>
            {
                new("Mug"),
                new("T-Shirt"),
                new("Sheet"),
                new("USB Memory Stick")
            };
    }

    static IEnumerable<CatalogItem> GetPreconfiguredItems()
    {
        return new List<CatalogItem>
            {
                new(2,2, ".NET Bot Black Sweatshirt", ".NET Bot Black Sweatshirt", 19.5M,  "http://catalogbaseurltobereplaced/images/products/1.png"),
                new(1,2, ".NET Black & White Mug", ".NET Black & White Mug", 8.50M, "http://catalogbaseurltobereplaced/images/products/2.png"),
                new(2,5, "Prism White T-Shirt", "Prism White T-Shirt", 12,  "http://catalogbaseurltobereplaced/images/products/3.png"),
                new(2,2, ".NET Foundation Sweatshirt", ".NET Foundation Sweatshirt", 12, "http://catalogbaseurltobereplaced/images/products/4.png"),
                new(3,5, "Roslyn Red Sheet", "Roslyn Red Sheet", 8.5M, "http://catalogbaseurltobereplaced/images/products/5.png"),
                new(2,2, ".NET Blue Sweatshirt", ".NET Blue Sweatshirt", 12, "http://catalogbaseurltobereplaced/images/products/6.png"),
                new(2,5, "Roslyn Red T-Shirt", "Roslyn Red T-Shirt",  12, "http://catalogbaseurltobereplaced/images/products/7.png"),
                new(2,5, "Kudu Purple Sweatshirt", "Kudu Purple Sweatshirt", 8.5M, "http://catalogbaseurltobereplaced/images/products/8.png"),
                new(1,5, "Cup<T> White Mug", "Cup<T> White Mug", 12, "http://catalogbaseurltobereplaced/images/products/9.png"),
                new(3,2, ".NET Foundation Sheet", ".NET Foundation Sheet", 12, "http://catalogbaseurltobereplaced/images/products/10.png"),
                new(3,2, "Cup<T> Sheet", "Cup<T> Sheet", 8.5M, "http://catalogbaseurltobereplaced/images/products/11.png"),
                new(2,5, "Prism White TShirt", "Prism White TShirt", 12, "http://catalogbaseurltobereplaced/images/products/12.png"),
                new(1, 5, "Cup White Mug", "Cup White Mug", 12, "http://catalogbaseurltobereplaced/images/products/9.png"), // Guayaquil
            };
    }

    static IEnumerable<CatalogLocalidad> GetPreconfiguredLocalidades()
    {
        return new List<CatalogLocalidad>
            {
                new("Quito"),
                new("Guayaquil"),
                new("Default"),
            };
    }


}
