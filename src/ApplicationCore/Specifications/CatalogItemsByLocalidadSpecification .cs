﻿using Ardalis.Specification;
using Microsoft.eShopWeb.ApplicationCore.Entities;

public class CatalogItemsByLocalidadSpecification : Specification<CatalogItem>
{
    public CatalogItemsByLocalidadSpecification(int? localidadId)
    {
        Query.Where(i => !localidadId.HasValue || i.CatalogLocalidadId == localidadId);
    }
}
