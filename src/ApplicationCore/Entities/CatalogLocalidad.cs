﻿using Microsoft.eShopWeb.ApplicationCore.Interfaces;

namespace Microsoft.eShopWeb.ApplicationCore.Entities;
public class CatalogLocalidad : BaseEntity, IAggregateRoot
{
    public string Localidad { get; private set; }
    public CatalogLocalidad(string localidad)
    {
        Localidad = localidad;
    }
}
